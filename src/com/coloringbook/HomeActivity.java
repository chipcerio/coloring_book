package com.coloringbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_home);
	}
	
	public void startPaint(View view) {
		if (view.getId() == R.id.btn_start_paint) {
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
			finish();
		}
	}

}
