package com.coloringbook;

import com.coloringbook.util.DebugLog;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

public class Settings extends DialogFragment implements OnCheckedChangeListener {
	private boolean hasSfx;
	private StartFirstPage mHome;
	private Switch mSfxSwitch;
	
	private static final String TAG = "Settings";
	
	public Settings(StartFirstPage home) {
		mHome = home;
	}

	@Override
	public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
		View view = i.inflate(R.layout.settings, c);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		hasSfx = ColoringBookApp.getInstance().getSfxFlag();
		init(view);
		
		return view;
	}
	
	private void init(View view) {
		mSfxSwitch = (Switch) view.findViewById(R.id.hidden_settings_background_switch);
		mSfxSwitch.setOnCheckedChangeListener(this);
		mSfxSwitch.setChecked(hasSfx);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.getId() == R.id.hidden_settings_background_switch) {
			if (isChecked) {
				mSfxSwitch.setChecked(true);
				mHome.mSfx.setVolume(1.0f, 1.0f);
				ColoringBookApp.getInstance().setSfxFlag(true);
				DebugLog.i(TAG, "isChecked="+ColoringBookApp.getInstance().getSfxFlag());
			} else {
				mSfxSwitch.setChecked(false);
				mHome.mSfx.setVolume(0.0f, 0.0f);
				ColoringBookApp.getInstance().setSfxFlag(false);
				DebugLog.i(TAG, "isChecked="+ColoringBookApp.getInstance().getSfxFlag());
			}
		}
		
	}

}
