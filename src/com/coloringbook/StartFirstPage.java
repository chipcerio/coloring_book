package com.coloringbook;


import com.coloringbook.util.DebugLog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class StartFirstPage extends Activity {
	protected MediaPlayer mSfx;
	private Settings mSettings;
	private boolean hasSfx;
	
	private static final String TAG = "StartFirstPage";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_start_page);
		
		mSettings = new Settings(this);
		
		hasSfx = getIntent().getBooleanExtra("sfx", true);
		ColoringBookApp.getInstance().setSfxFlag(hasSfx);
		applySfx();
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		applySfx();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
		}
	}

	private void applySfx() {
		mSfx = MediaPlayer.create(this, R.raw.sfx00);
		mSfx.setLooping(true);
		mSfx.start();
		
		hasSfx = ColoringBookApp.getInstance().getSfxFlag();
		if(hasSfx)
			mSfx.setVolume(1.0f, 1.0f);
		else
			mSfx.setVolume(0.0f, 0.0f);
	}

	public void showMain(View view) {
		switch (view.getId()) {
		case R.id.btn_exit:
			finish();
			break;

		case R.id.btn_settings:
			hasSfx = ColoringBookApp.getInstance().getSfxFlag();
			mSettings.show(getFragmentManager(), "settings_fragment");
			break;
			
		case R.id.btn_start_paint:
			if (mSfx.isPlaying()) {
				mSfx.stop();
				mSfx.release();
				mSfx = null;
			}
			
			DebugLog.i(TAG, "isChecked="+ColoringBookApp.getInstance().getSfxFlag());
			
			Intent intent = new Intent(this, MainActivity.class);
			intent.putExtra("sfx", ColoringBookApp.getInstance().getSfxFlag());
			startActivity(intent);
			break;
			
		case R.id.btn_main_gallery:
			Intent i = new Intent(Intent.ACTION_PICK);
			i.setType("image/*");
			startActivityForResult(i, 1);
			break;

		}

	}
}
