package com.coloringbook;

import android.app.Application;
import android.content.SharedPreferences;

public class ColoringBookApp extends Application {
	private static SharedPreferences mPref;
	private static ColoringBookApp mInstance;
	
	private static final String USER_PREF = "user_pref";
	private static final String SFX_PREF  = "sfx_pref"; 

	public ColoringBookApp() {
		super();
		mInstance = this;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
	}

	public static ColoringBookApp getInstance() {
		if (mInstance == null) {
			synchronized (ColoringBookApp.class) {
				if (mInstance == null)
					new ColoringBookApp();
			}
		}
		
		if (mPref == null)
			mPref = mInstance.getSharedPreferences(USER_PREF, MODE_PRIVATE);
		
		return mInstance;
	}
	
	public void setSfxFlag(boolean hasBackground) {
		SharedPreferences.Editor editor = mPref.edit();
		editor.putBoolean(SFX_PREF, hasBackground);
		editor.commit();
	}
	
	public boolean getSfxFlag() {
		return mPref.getBoolean(SFX_PREF, true);
	}
	
}
