package com.coloringbook.util;

import android.util.Log;

public class DebugLog {
	public static final boolean DEBUG_ON = false; // toggle to false during release

	public static int d(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.d(tag, msg);
		return response;
	}

	public static int e(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.e(tag, msg);
		return response;
	}

	public static int i(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.i(tag, msg);
		return response;
	}

	public static int v(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.v(tag, msg);
		return response;
	}

	public static int w(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.w(tag, msg);
		return response;
	}

}