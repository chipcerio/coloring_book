package com.coloringbook;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.coloringbook.util.DebugLog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Images;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends Activity implements PaintView.LifecycleListener {
	private boolean _saveInProgress;
	private boolean hasSfx;
	private PaintView _paintView;
	private ProgressBar _progressBar;
	private ProgressDialog _progressDialog;
	private State _state;
	private MediaPlayer mSfx;
	
	private static final int REQUEST_START_NEW = 0;
	private static final int DIALOG_PROGRESS = 1;
	private static final int SAVE_DIALOG_WAIT_MILLIS = 1500;
	private static final String MIME_PNG = "image/png";
	public static final String INTENT_START_NEW = "com.coloringbook.paint.START_NEW";
	
	public MainActivity() {
		_state = new State();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);
		
		hasSfx = getIntent().getBooleanExtra("sfx", true);
		ColoringBookApp.getInstance().setSfxFlag(hasSfx);
		applySfx();
		
		_paintView = (PaintView) findViewById(R.id.paint_view);
		_paintView.setLifecycleListener(this);
		_progressBar = (ProgressBar) findViewById(R.id.paint_progress);
		_progressBar.setMax(Progress.MAX);
		
		final Object previousState = getLastNonConfigurationInstance();
		if (previousState == null) {
			_paintView.setVisibility(View.INVISIBLE);
			_progressBar.setVisibility(View.GONE);
		} else {
			SavedState state = (SavedState) previousState;
			_state = state._paintActivityState;
			_paintView.setState(state._paintViewState);
			_paintView.setVisibility(View.VISIBLE);
			_progressBar.setVisibility(View.GONE);
			if (_state._loadInProgress)
				new InitPaintView(_state._loadedResourceId);
		}
		
		_paintView.setPaintColor(Color.WHITE);
	}
	
	private void applySfx() {
		mSfx = MediaPlayer.create(this, R.raw.sfx00);
		mSfx.setLooping(true);
		mSfx.start();
		
		if (hasSfx)
			mSfx.setVolume(1.0f, 1.0f);
		else
			mSfx.setVolume(0.0f, 0.0f);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		applySfx();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
		}
	}

	@Override
	public void onPreparedToLoad() {
		new Handler() {
			@Override
			public void handleMessage(Message m) {
				new InitPaintView(StartNewActivity.randomOutlineId());
			}
		}.sendEmptyMessage(0);
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		SavedState state = new SavedState();
		state._paintActivityState = _state;
		state._paintViewState = _paintView.getState();
		return state;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_START_NEW:
			if (resultCode != 0)
				new InitPaintView(resultCode);
			break;

		default:
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_PROGRESS:
			_progressDialog = new ProgressDialog(this);
			_progressDialog.setCancelable(false);
			_progressDialog.setIcon(android.R.drawable.ic_dialog_info);
			_progressDialog.setTitle(R.string.dialog_saving);
			_progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			_progressDialog.setMax(Progress.MAX);
			
			if (!_saveInProgress) {
				new Handler() {
					@Override
					public void handleMessage(Message m) {
						_progressDialog.dismiss();
					}
				}.sendEmptyMessage(0);
			}
			
			return _progressDialog;
		}
		
		return null;
	}

	// Trash button clicked
	public void requestNew(View view) {
		if (view.getId() == R.id.btn_trash)
			startActivityForResult(new Intent(INTENT_START_NEW), REQUEST_START_NEW);
	}
	
	public void selectColor(View view) {
		switch (view.getId()) {
		case R.id.btn_black:
			_paintView.setPaintColor(Color.BLACK);
			break;
		case R.id.btn_blue:
			_paintView.setPaintColor(Color.BLUE);
			break;
		case R.id.btn_cyan:
			_paintView.setPaintColor(Color.CYAN);
			break;
		case R.id.btn_orange:
			_paintView.setPaintColor(Color.rgb(255, 153, 0));
			break;
		case R.id.btn_yellow:
			_paintView.setPaintColor(Color.YELLOW);
			break;
		case R.id.btn_green:
			_paintView.setPaintColor(Color.GREEN);
			break;
		case R.id.btn_teal:
			_paintView.setPaintColor(Color.rgb(9, 112, 84));
			break;
		case R.id.btn_magenta:
			_paintView.setPaintColor(Color.MAGENTA);
			break;
		case R.id.btn_pink:
			_paintView.setPaintColor(Color.rgb(255, 186, 210));
			break;
		case R.id.btn_red:
			_paintView.setPaintColor(Color.RED);
			break;
		case R.id.btn_brown:
			_paintView.setPaintColor(Color.rgb(102, 51, 0));
			break;
		case R.id.btn_eraser:
			_paintView.setPaintColor(Color.WHITE);
			break;
		default:
			break;
		}
	}
	
	private static final String TAG = "MainActivity";
	
	public void gotoHome(View view) {
		if (view.getId() == R.id.btn_home) {
			if (mSfx.isPlaying()) {
				mSfx.stop();
				mSfx.release();
				mSfx = null;
			}
			
			DebugLog.i(TAG, "sfx="+ColoringBookApp.getInstance().getSfxFlag());
			Intent i = new Intent(this, StartFirstPage.class);
			i.putExtra("sfx", ColoringBookApp.getInstance().getSfxFlag());
			startActivity(i);
			finish();
		}
	}
	
	public void openGallery(View view) {
		if (view.getId() == R.id.btn_gallery) {
			Intent i = new Intent(Intent.ACTION_PICK);
			i.setType("image/*");
			startActivityForResult(i, 1);
		}
	}
	
	public void saveArtwork(View view) {
		if (view.getId() == R.id.btn_save)
			new BitmapSaver();
	}
	
	private class InitPaintView implements Runnable {
		private Bitmap _originalOutlineBitmap;
		private Handler _handler;
		
		public InitPaintView(int outlineResourceId) {
			_paintView.setVisibility(View.GONE);
			_progressBar.setProgress(0);
			_progressBar.setVisibility(View.VISIBLE);
			_state._savedImageUri = null;
			
			_state._loadInProgress = true;
			_state._loadedResourceId = outlineResourceId;
			_originalOutlineBitmap = BitmapFactory.decodeResource(getResources(), outlineResourceId);
			
			_handler = new Handler() {
				@Override
				public void handleMessage(Message m) {
					switch (m.what) {
					case Progress.MESSAGE_INCREMENT_PROGRESS:
						_progressBar.incrementProgressBy(m.arg1);
						break;
					case Progress.MESSAGE_DONE_OK:
					case Progress.MESSAGE_DONE_ERROR:
						_state._loadInProgress = false;
						_paintView.setVisibility(View.VISIBLE);
						_progressBar.setVisibility(View.GONE);
						break;
					default:
						break;
					}
				}
			};
			
			new Thread(this).start();
		}
		
		@Override
		public void run() {
			_paintView.loadFromBitmap(_originalOutlineBitmap, _handler);
		}
		
	}
	
	private class MediaScannerNotifier implements MediaScannerConnectionClient {
		private MediaScannerConnection _connection;
		private String _path;
		private String _mimeType;
		
		public MediaScannerNotifier(Context context, String path, String mimeType) {
			_path = path;
			_mimeType = mimeType;
			_connection = new MediaScannerConnection(context, this);
			_connection.connect();
		}

		@Override
		public void onMediaScannerConnected() {
			_connection.scanFile(_path, _mimeType);
		}

		@Override
		public void onScanCompleted(String path, Uri uri) {
			_connection.disconnect();
		}
		
	}
	
	private class BitmapSaver implements Runnable {
		private Bitmap _originalOutlineBitmap;
		private String _filename;
		private File _file;
		private Handler _progressHandler;
		private Uri _newImageUri;
		
		public BitmapSaver() {
			class DelayHandler extends Handler {
				@Override
				public void handleMessage(Message m) {
				  // We are done, hide the progress bar and turn 
					// the paint view back on.
					_saveInProgress = false;
					_progressDialog.dismiss();
				}
			}
			
			class ProgressHandler extends Handler {
				@Override
				public void handleMessage(Message m) {
					switch (m.what) {
					case Progress.MESSAGE_INCREMENT_PROGRESS:
						// Update progress bar.
						_progressDialog.incrementProgressBy(m.arg1);
						break;
					case Progress.MESSAGE_DONE_OK:
					case Progress.MESSAGE_DONE_ERROR:
						if (m.what == Progress.MESSAGE_DONE_OK)
							finishSaving();
						String title = getString(R.string.dialog_saving);
						if (m.what == Progress.MESSAGE_DONE_OK)
							title += getString(R.string.dialog_saving_ok);
						else
							title += getString(R.string.dialog_saving_error);
						_progressDialog.setTitle(title);
						new DelayHandler().sendEmptyMessageDelayed(0, 
								SAVE_DIALOG_WAIT_MILLIS);
						break;
					}
				}
			}
			
			if (_paintView.isInitialized()) {
				_saveInProgress = true;
				showDialog(DIALOG_PROGRESS);
				_progressDialog.setTitle(R.string.dialog_saving);
				_progressDialog.setProgress(0);
				_originalOutlineBitmap = BitmapFactory.decodeResource(getResources(), 
						_state._loadedResourceId);
				_progressHandler = new ProgressHandler();
				new Thread(this).start();
			}
		}

		@Override
		public void run() {
			// Get a filename.
			_filename = newImageFileName();
			_file = new File(Environment.getExternalStorageDirectory(), 
					getString(R.string.saved_image_path_prefix) + _filename + ".png");
			
			// Save the bitmap to a file.
			_paintView.saveToFile(_file, _originalOutlineBitmap, _progressHandler);
		}
		
		private void finishSaving() {
			// Save it to the MediaStore.
			ContentValues values = new ContentValues();
			values.put(Images.Media.TITLE, _filename);
			values.put(Images.Media.DISPLAY_NAME, _filename);
			values.put(Images.Media.MIME_TYPE, MIME_PNG);
			values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
			values.put(Images.Media.DATA, _file.toString());
			File parentFile = _file.getParentFile();
			values.put(Images.Media.BUCKET_ID, 
					parentFile.toString().toLowerCase().hashCode());
			values.put(Images.Media.BUCKET_DISPLAY_NAME, 
					parentFile.getName().toLowerCase());
			_newImageUri = getContentResolver().insert(
					Images.Media.EXTERNAL_CONTENT_URI, values);
			
			// Delete the old version. if we have any
			if (_state._savedImageUri != null) {
				getContentResolver().delete(_state._savedImageUri, null, null);
			}
			_state._savedImageUri = _newImageUri;

		  // Scan the file so that it appears in the system as it should.
			if (_newImageUri != null) {
				new MediaScannerNotifier(MainActivity.this, _file.toString(), MIME_PNG);
			}
		}
		
		private String newImageFileName() {
			final DateFormat fmt = new SimpleDateFormat("yyyyMMdd-HHmmss");
			return fmt.format(new Date());
		}
		
	}
	
	private static class SavedState {
		public State _paintActivityState;
		public Object _paintViewState;
	}
	
	private static class State {
		public boolean _loadInProgress;
		public int _loadedResourceId;
		public Uri _savedImageUri;
	}

}